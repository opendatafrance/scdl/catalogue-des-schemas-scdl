# Schémas du Socle Commun des Données Locales (SCDL)

Ce dépôt sert à indexer les schémas du SCDL afin de permettre leur documentation et leur utilisation dans [Validata](https://validata.fr/).

Le SCDL est un dispositif d'[OpenDataFrance](http://opendatafrance.net/) visant à définir un ensemble de jeux de données prioritaires, normalisés et communs aux collectivités territoriales françaises. Chaque jeu de données identifié fait l'objet d'un schéma : un document décrivant la structure, les champs et les contenus attendus.

Les schémas sont écrits au format [Table Schema](https://frictionlessdata.io/specs/table-schema/). À partir du fichier JSON, une documentation est générée automatiquement au format Markdown et anciennement publiée sur scdl.opendatafrance.net
(remplacé par [schema.data.gouv](https://schema.data.gouv.fr/) et [guides.gouv.fr](https://guides.data.gouv.fr/)). ([Plus d'informations sur ce dépôt dédié](https://gitlab.com/opendatafrance/scdl/documentation))

Le fichier [catalog.json](./catalog.json) réference tous les schémas du SCDL. Le processus de contribution au SCDL (ajout, modification, mise à jour des schémas) est décrit dans le fichier [CONTRIBUTING.md](./CONTRIBUTING.md).
