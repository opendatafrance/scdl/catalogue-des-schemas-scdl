# Comment contribuer au Socle commun des données locales (SCDL) ?

Tous les schémas du SCDL sont référencés dans le [Catalogue des schémas SCDL](https://gitlab.com/opendatafrance/scdl/catalog/).

L'ajout d'un schéma au catalogue se fait en proposant d'amender le fichier [catalog.json](https://gitlab.com/opendatafrance/scdl/catalog/blob/master/catalog.json) sous forme de _merge request_.

## Critères d'acceptation

[OpenDataFrance](http://opendatafrance.net/) se réserve le droit d'accepter ou de refuser l'ajout d'un schéma au sein du SCDL.

Pour faire partie du SCDL, un schéma doit :
- être écrit en JSON conformément aux spécifications [Table Schema](https://frictionlessdata.io/specs/table-schema/). Les métadonnées du schéma doivent employer [les propriétés qui ont été standardisées](https://github.com/frictionlessdata/specs/blob/master/specs/patterns.md#table-schema-metadata-properties), avec au minimum :
  - `name` : identifiant, ou "slug" ;
  - `title` : nom courant ;
  - `description` ;
  - `homepage` : page d'accueil ou dépôt Git ;
  - `path` : URL de cette version du schéma ;
  - `created` : date de création ;
  - `lastModified` : date de publication de cette version ;
  - `version` : le numéro de version au format [SemVer](https://semver.org/lang/fr/) ;
- être nommé `schema.json` ;
- être accompagné d'un fichier `README.md` complet, présentant notamment le contexte de sa création ;
- être mis à disposition en ligne sur une URL stable.

Optionnel :
- être mis à disposition dans un dépôt Git afin de bénéficier de la gestion des versions (voir ci-dessous) ;
- un fichier `CHANGELOG.md` peut être publié au même endroit que le fichier `schema.json` afin d'apporter des informations sur les différentes mises à jour du schéma ;
- des données tabulaires d'exemple sous forme de fichiers CSV, valides ou invalides, peuvent également être ajoutées dans un dossier séparé (et référencées dans le schéma grâce à la propriété `resources`).

## Cycle de vie d'un schéma SCDL

### Contribution

Concerne la création initiale ou la mise à jour d'un schéma.

Une contribution se fait toujours dans une branche, la branche `master` étant protégée. Plusieurs branches peuvent coexister lorsque différentes évolutions n'ont pas la même nature.

Lorsqu'un changement est cassant, penser à mettre à jour le fichier d'exemple valide pour qu'il reste valide.

Le contributeur ne doit pas nécessairement mettre à jour `CHANGELOG.md` ni le numéro de version dans `schema.json` (à faire lors de l'intégration et de la publication).

Lorsque le contributeur n'est pas membre de l'équipe, il travaillera dans un fork du dépôt du schéma.

Lorsqu'une branche est prête à être proposée, le contributeur ouvre une _merge request_.

### Intégration

Concerne la revue et l'acceptation des changements proposés par un contributeur.

L'intégrateur est celui qui a les droits d'écriture sur la branche `master`. Après vérification de la _merge request_, il peut fusionner la branche dans `master`. Ainsi `master` accumule un certain nombre de modifications par rapport à la dernière version publiée.

Conséquence : le fichier `catalog.json` ne doit pas référencer les schémas depuis `master` mais depuis des URLs contenant un _tag_ (cf ci-dessous).

L'intégrateur doit tenir à jour une section prévisionnelle dans le `CHANGELOG.md` de type "X.Y.Z -> next" où "X.Y.Z" est la dernière version publiée et "next" est un marqueur qui sera remplacé par le nouveau numéro de version lors de la publication. Cette section doit être placée tout en haut du fichier (le plus récent en premier).

### Publication d'une version ("release")

Cette dernière étape consiste à rendre visibles et utilisables, notamment sur Validata, les changements accumulés dans `master` depuis la dernière version publiée. Il s'agit de figer, à un instant _t_ et dans un ensemble cohérent, tous les fichiers du dépôt du schéma (schéma, documentation, fichiers exemples, etc.).

Les schémas SCDL suivent le principe du [_semantic versioning_](https://semver.org/lang/fr/).

L'objectif est d'attribuer un nouveau numéro de version au schéma. Pour cela :

- déterminer si l'ensemble des modifications depuis la version précédente est rétrocompatible ou non ("cassant") ;
- mettre à jour le numéro de version en conséquence (majeur, mineur ou patch) dans `schema.json`:
  - `version` ;
  - `lastModified` : date de mise à jour ;
  - `path` et `resources` : mise à jour de l'URL avec le bon _tag_ de version ;
- ajouter ou modifier l'entrée dans le `CHANGELOG.md` (le plus récent en haut) qui décrit les changements rétrocompatibles et non-rétrocompatibles dans 2 listes séparées ([exemple](https://gitlab.com/opendatafrance/scdl/adresses/blob/v1.1.2/CHANGELOG.md) ;
- faire un _commit_ de _release_ qui ne contienne que les 2 points précédents, dont le message est "Version X.Y.Z" (remplacer X.Y.Z) ;
- _tagger_ ce _commit_ avec `vX.Y.Z` (remplacer X.Y.Z) ;
- _pusher_ 2 fois : `git push origin master` et `git push --tags`.
